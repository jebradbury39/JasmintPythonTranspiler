import time

# so we can add attributes to instances
# a = Custom_List([1,2,3]).setRef("array{int}")
# setRef is optional
class Custom_List(list):
   def setRef(self, typeStr, isInnerRef, isCopy):
      self.typeStr = typeStr
      self.isInnerRef = isInnerRef
      self.isCopy = isCopy
      return self
      
   def copy(self):
      newInstance = Custom_List().setRef(self.typeStr, self.isInnerRef, self.isCopy)
      for item in self:
         newItem = item
         if self.isCopy:
            newItem = newItem.copy()
         newInstance.append(newItem)
      return newInstance

   def size(self):
      return len(self)

class Custom_Dict(dict):
   def setRef(self, typeStr, isKeyRef, isValRef, isCopyKey, isCopyVal):
      self.typeStr = typeStr
      self.isKeyRef = isKeyRef
      self.isValRef = isValRef
      self.isCopyKey = isCopyKey
      self.isCopyVal = isCopyVal
      return self
      
   def copy(self):
      newInstance = Custom_Dict().setRef(self.typeStr, self.isKeyRef, self.isValRef, self.isCopyKey, self.isCopyVal)
      for key in self:
         newKey = key
         newItem = self[key]
         if self.isCopyKey:
            newKey = newKey.copy()
         if self.isCopyVal:
            newItem = newItem.copy()
         newInstance[newKey] = newItem
      return newInstance
   
'''
Exeption* ptr = None
fnCall(Custom_Ptr(ptr))
//this fails, since we have no ref to the class instance
Exception* ptr = X()
fnCall(Custom_Ptr(ptr))
//this would be fine, since we can access the class

Exception* ptr = null;
*ptr = X(); //fail
Exception** pre_exc = &ptr;
*pre_exc = new Exception();
if (ptr != null) {
}

memory[4] = None; //ptr is stored at 4
memory[memory[4]] = X() //error, since memory[4] contains None
memory[5] = 4; //pre_exc is stored at 5
memory[memory[5]] = new Exception(); //exception value goes into memory[6], and returns 6
in memory
memory[4] (ptr) is now holding 6, which is the memory location of the exception

ptr = Ptr(None) //memory[4] = None, ptr.ref.ref
if ptr.ref == None:
   //fail
ptr.ref = X()
replace_with(ptr, X()) # should fail, since attempting to deref null
pre_exc = Ptr(ptr) # pre_exc.ref is ptr

if pre_exc.ref == None:
   //fail
pre_exc.ref = Ptr(Exception()) # no fail, since source is ptr


replace_with(pre_exc, Exception())

if ptr.ref != None:
   pass

Exception** pre_exc = null;
Exception*** pre_exc2 = pre_exc;
**pre_exc2 = X() //fail
*pre_exc2 = X()

pre_exc = Ptr(None)
pre_exc2 = Ptr(pre_exc)
replace_with(pre_exc2.ref, X())
pre_exc.update(

ptr = Nullptr()
&ptr becomes Custom_Ptr(ptr) # {self.ref = ptr}
self.ref = X() # no good, since ptr still has not changed
self.ref.update(X()) # this might work, but not very nice

note that none of these schemes are comprehensive, so we try to avoid
'''

def custom_print(msg):
  print(msg, end='', flush=True)

def to_string(obj, isRef):
   if hasattr(obj, "typeStr") and isRef:
      return "<reference::" + obj.typeStr + ">"
      
   if isinstance(obj, dict):
      kvStr = []
      for key in obj:
         kvStr.append(to_string(key, obj.isKeyRef) + ": " + to_string(obj[key], obj.isValRef))
      kvStr.sort()
      str_value = "{"
      first = True
      for kv in kvStr:
         if not first:
            str_value += ", "
         first = False
         str_value += kv
      return str_value + "}"
   elif isinstance(obj, list):
      str_value = "["
      first = True
      for element in obj:
         if not first:
            str_value += ", "
         first = False
         str_value += to_string(element, obj.isInnerRef)
      return str_value + "]"
   elif isinstance(obj, bool):
      if obj:
         return "true"
      else:
         return "false"
   else:
      if obj == None:
         return "null"
      return str(obj)

#only used for assignment to LvalueDereference
def replace_with(target, source):
   if target == None:
      # null reference
      raise ValueError("Cannot dereference null. In C, this would be a SIGSEGV")
   elif isinstance(target, list):
      target.clear()
      target.extend(source)
   elif isinstance(target, dict):
      target.clear()
      target.update(source)
   else:
      target.replace_with(source)
   
class Namespace:
   def __init__(self, **kwargs):
      self.__dict__.update(kwargs)
      self.kwargs = kwargs

   def add(self, key, value):
      self.__dict__[key] = value

   def copy(self):
      return Namespace(**self.kwargs)

def custom_ord(char):
   if isinstance(char, str):
      return ord(char)
   return char

def custom_chr(num):
   if isinstance(num, int):
      return chr(num)
   return num

def currentUnixEpochTime():
   return time.time()
