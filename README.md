# JasmintPython

This project produces an executable which takes in a `.bson` file containing a serialized Jasmint AST and produces equivalent python code in `py_out.py`.

### Building

1. Build the Jasmint library
2. Copy `jasmint.jar` into `lib/`
3. Run `ant`
4. Run `java -jar dist/jasmint-python.jar <filename>`

### Running the Transpiled Code

Simply run `python3 py_out.py`.