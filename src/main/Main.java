package main;

import errors.FatalMessageException;
import interp.InterpError;
import transpiler.TranspileProject;
import transpiler.TranspilerError;

public class Main {
  private static String builtinFname = "jsmnt_python_builtin/builtin.py";
  private static String inputFile = null;

  public static void main(String[] args) {
    parseParameters(args);

    if (inputFile == null) {
      System.out.println("py_transpiler: no input ast.bson file specified");
      System.exit(1);
    }
    TranspileProject transpiler = new TranspileProject(builtinFname, inputFile);
    try {
      transpiler.run();
    } catch (InterpError | FatalMessageException | InterruptedException | TranspilerError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private static void parseParameters(String[] args) {
    for (int i = 0; i < args.length; i++) {
      if (args[i].charAt(0) == '-') {
        if (args[i].equals("-builtin")) {
          if (i + 1 < args.length) {
            builtinFname = args[i + 1];
            i++;
          } else {
            System.err.println("-builtin requires a file path");
            System.exit(1);
          }
        } else {
          System.err.println("unexpected option: " + args[i]);
          System.exit(1);
        }
      } else if (inputFile != null) {
        System.err.println("too many files specified");
        System.exit(1);
      } else {
        inputFile = args[i];
      }
    }
  }
}
