package transpiler;

import ast.AbstractAst.AstType;
import ast.AbstractExpression;
import ast.AbstractLvalue;
import ast.AbstractStatement;
import ast.ArrayInitExpression;
import ast.AssignmentStatement;
import ast.Ast;
import ast.BinaryExpression;
import ast.BlockStatement;
import ast.BoolExpression;
import ast.BracketExpression;
import ast.BreakStatement;
import ast.CastExpression;
import ast.CharExpression;
import ast.ClassDeclaration;
import ast.ClassDeclaration.ClassLine;
import ast.CommentStatement;
import ast.ConditionalStatement;
import ast.Declaration;
import ast.DeclarationStatement;
import ast.DereferenceExpression;
import ast.DotExpression;
import ast.EnumDeclaration;
import ast.Expression;
import ast.ExpressionStatement;
import ast.FfiExpression;
import ast.FfiStatement;
import ast.FloatExpression;
import ast.ForStatement;
import ast.Function;
import ast.Function.MetaType;
import ast.FunctionCallExpression;
import ast.FunctionCallStatement;
import ast.IdentifierExpression;
import ast.ImportStatement;
import ast.InstanceofExpression;
import ast.IntegerExpression;
import ast.LambdaExpression;
import ast.LeftUnaryOpExpression;
import ast.Lvalue;
import ast.LvalueBracket;
import ast.LvalueDereference;
import ast.LvalueDot;
import ast.LvalueFfi;
import ast.LvalueId;
import ast.MapInitExpression;
import ast.MultiExpression;
import ast.MultiLvalue;
import ast.NewClassInstanceExpression;
import ast.NewExpression;
import ast.Program;
import ast.ReferenceExpression;
import ast.ReturnStatement;
import ast.SizeofExpression;
import ast.Statement;
import ast.StaticClassIdentifierExpression;
import ast.StringExpression;
import ast.SubmodStatement;
import ast.VtableAccessExpression;
import ast.WhileStatement;
import audit.AuditEntry;
import audit.AuditEntry.AuditEntryType;
import audit.AuditRenameTypes;
import audit.AuditTrailLite;
import errors.FatalMessageException;
import errors.MsgState;
import errors.Nullable;
import header.ClassHeader;
import header.ClassHeader.ClassMember;
import import_mgmt.EffectiveImports;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import mbo.Renamings.RenamingIdKey;
import mbo.Vtables.ClassVtable;
import mbo.Vtables.VtableEntry;
import multifile.JsmntGlobal;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import type_env.VariableScoping;
import typecheck.AbstractType;
import typecheck.ArrayType;
import typecheck.CharType;
import typecheck.ClassDeclType;
import typecheck.ClassType;
import typecheck.EnumDeclType;
import typecheck.EnumType;
import typecheck.FunctionType;
import typecheck.MapType;
import typecheck.ModuleType;
import typecheck.MultiType;
import typecheck.ReferenceType;
import typecheck.StringType;
import typecheck.Type;
import typecheck.UserInstanceType;
import typecheck.VoidType;

public class TranspileProgram {
  public final String LANG = "Python";

  private final String builtinFname;
  private final Project project;
  private final Program program;
  private final String pyFname;
  private final String subPyFname; // without leading "build-py"
  private static final String INCREASE_INDENT = "   ";
  private String indent = "";
  private final EffectiveImports effectiveImports;

  private List<String> collectedGlobals = new LinkedList<>();
  private String topLevelFuncs = "";

  private ClassDeclType rootObjType = null;
  private EnumDeclType cmpResultType = null;
  private String cmpEqId = "";

  public TranspileProgram(String builtinFname, Project project, Program program,
      EffectiveImports effectiveImports) {
    this.builtinFname = builtinFname;
    this.project = project;
    this.program = program;
    // need to build into a subdir for a library project (to avoid collisions
    // between different versions of a dependency)
    String projectName = project.dirPath();
    String progName = program.moduleStatement.fullType.toString();
    this.subPyFname = projectName + "/" + progName + ".py";
    this.pyFname = "build-py/" + this.subPyFname;
    this.effectiveImports = effectiveImports;
  }

  private String printGlobals() {
    String result = "";
    for (String global : collectedGlobals) {
      result += indent + "global " + global + "\n";
    }
    return result;
  }

  private void getJsmntGlobalRenamings() throws FatalMessageException, TranspilerError {
    Nullable<ModuleEndpoint> modEp = project.lookupModule(JsmntGlobal.modType,
        new AuditTrailLite());
    if (modEp.isNull()) {
      throw new TranspilerError("unable to find jsmnt_global");
    }
    modEp.get().load();
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();

    Nullable<AuditEntry> globalRenames = moduleHandle.moduleBuildObj.auditTrail
        .getCombined(AuditEntryType.RENAME_TYPES);
    if (globalRenames.isNull()) {
      throw new TranspilerError("unable to find type renames");
    }
    AuditRenameTypes globalRenaming = (AuditRenameTypes) globalRenames.get();
    Type tmp = globalRenaming.typeRenamings
        .get(new EnumDeclType(Nullable.of(JsmntGlobal.modType), "CmpResult"));
    if (tmp == null) {
      throw new TranspilerError("unable to find cmp enum in type renames");
    }
    this.cmpResultType = (EnumDeclType) tmp;
    Optional<Nullable<Type>> tmpNorm = cmpResultType.basicNormalize(true,
        program.moduleStatement.fullType, effectiveImports, new MsgState(Nullable.empty()));
    if (!tmpNorm.isPresent()) {
      throw new TranspilerError("unable to normalize enum type: " + cmpResultType);
    }
    cmpResultType = (EnumDeclType) tmpNorm.get().get();

    tmp = globalRenaming.typeRenamings
        .get(new ClassDeclType(Nullable.of(JsmntGlobal.modType), "Obj"));
    if (tmp == null) {
      throw new TranspilerError("unable to find Obj class in type renames");
    }
    this.rootObjType = (ClassDeclType) tmp;
    tmpNorm = rootObjType.basicNormalize(true, program.moduleStatement.fullType, effectiveImports,
        new MsgState(Nullable.empty()));
    if (!tmpNorm.isPresent()) {
      throw new TranspilerError("unable to normalize root obj type: " + cmpResultType);
    }
    rootObjType = (ClassDeclType) tmpNorm.get().get();

    // now look up EQ
    RenamingIdKey idKey = new RenamingIdKey(
        Nullable.of(new EnumDeclType(Nullable.of(JsmntGlobal.modType), "CmpResult")),
        Nullable.empty(), "EQ");
    RenamingIdKey newIdKey = idKey.applyTransforms(moduleHandle.moduleBuildObj.auditTrail);

    this.cmpEqId = newIdKey.id;
  }

  public void transpileProgram() throws IOException, FatalMessageException, TranspilerError {
    File outFile = new File(pyFname).getAbsoluteFile();
    if (!outFile.exists()) {
      String dir = outFile.getParent();
      if (dir != null) {
        new File(dir).mkdirs();
        // make this directory a module
        File initFile = new File(dir + "/__init__.py").getAbsoluteFile();
        initFile.createNewFile();
      }
      outFile.createNewFile();
    }

    FileOutputStream out = new FileOutputStream(outFile);

    // find jsmnt global renames
    getJsmntGlobalRenamings();

    /* add common imports. In future can check for which ones are actually needed */
    // copy.deepcopy(obj)
    out.write(("import sys\n" + "import os\n" + "from enum import Enum\n" + "import copy\n"
        + "PARENT_DIR = os.path.dirname(os.path.realpath(__file__))\n" + "\n").getBytes());

    // run through program lines and collect all declarations (class, fn, var).
    // transpile those first
    List<Ast> declarations = new LinkedList<Ast>();
    List<Ast> body = new LinkedList<Ast>();

    for (Ast ast : program.getLines()) {
      if (ast instanceof ClassDeclaration || ast instanceof Function || ast instanceof FfiStatement
          || ast instanceof EnumDeclaration) {
        declarations.add(ast);
      } else {
        body.add(ast);
      }
    }

    // add built-in functions
    out.write(addBuiltInFunctions(builtinFname).getBytes());

    // transpile imports (might be from other projects)
    String imports = "";
    for (ImportStatement imp : program.imports) {
      Nullable<ModuleEndpoint> modEp = project.lookupModule(imp.target, project.getAuditTrail());
      if (modEp.isNull()) {
        // scream about it
      }
      String projectDir = modEp.get().project.dirPath();
      String importPath = imp.target.toList().get(0);
      String fullPath = projectDir + "." + importPath;
      imports += "import " + fullPath + " as " + importPath + "\n";
    }
    out.write(imports.getBytes());

    // transpile submods
    String submods = "";
    if (!program.submods.isEmpty()) {
      submods += "import importlib.machinery\n" + "import importlib.util\n";
    }
    // all submods are in the same Project
    for (SubmodStatement submod : program.submods) {
      String fullName = program.moduleStatement.fullType.toString() + "."
          + submod.absSubmodPath.name;
      submods += "loader = importlib.machinery.SourceFileLoader('m', os.path.join(PARENT_DIR, '"
          + fullName + ".py'))\n" + "spec = importlib.util.spec_from_loader(loader.name, loader)\n"
          + submod.absSubmodPath.name + " = importlib.util.module_from_spec(spec)\n"
          + "loader.exec_module(" + submod.absSubmodPath.name + ")\n";
    }
    out.write(submods.getBytes());

    // transpile declarations first, in global scope
    // transpile everything else into main
    try {
      String gDeclTmp = transpileGlobalDeclarations(program, declarations);
      String mainTmp = transpileMain(program);
      out.write(topLevelFuncs.getBytes());
      out.write(gDeclTmp.getBytes());
      out.write(mainTmp.getBytes());
      if (!mainTmp.isEmpty()) {

        FileOutputStream mainOut = new FileOutputStream("build-py/main.py");
        String mainOutRes = "#!/usr/bin/env python3\n";
        mainOutRes += "import importlib.machinery\nimport importlib.util\nimport signal\nimport sys\n";
        mainOutRes += "def signal_handler(sig, frame):\n";
        mainOutRes += INCREASE_INDENT + "print('')\n";
        mainOutRes += INCREASE_INDENT + "sys.exit(0)\n";
        mainOutRes += "signal.signal(signal.SIGINT, signal_handler)\n\n";
        mainOutRes += "import os\n";
        mainOutRes += "PARENT_DIR = os.path.dirname(os.path.realpath(__file__))\n";
        mainOutRes += "loader = importlib.machinery.SourceFileLoader('m', os.path.join(PARENT_DIR, '"
            + subPyFname + "'))\n" + "";
        mainOutRes += "spec = importlib.util.spec_from_loader(loader.name, loader)\n";
        mainOutRes += "m = importlib.util.module_from_spec(spec)\n";
        mainOutRes += "loader.exec_module(m)\n";
        mainOutRes += "if __name__ == '__main__':\n" + INCREASE_INDENT + "m.main()\n";
        mainOut.write(mainOutRes.getBytes());
        mainOut.close();

        out.write(("if __name__ == \"__main__\":\n" + INCREASE_INDENT + "main()").getBytes());
      }
    } catch (TranspilerError e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    out.close();
    System.out.println("  -> " + outFile);
  }

  private String addBuiltInFunctions(String builtinFname) {
    String result = "";
    try {
      result = new String(Files.readAllBytes(Paths.get(builtinFname)));
    } catch (IOException e) {
      e.printStackTrace();
    }

    return result + "\n";
  }

  private String transpileGlobalDeclarations(Program program, List<Ast> declarations)
      throws TranspilerError, FatalMessageException {
    String out = "";

    // transpile all enums first, since they have no dependency on anything else
    for (Ast ast : declarations) {
      if (ast instanceof EnumDeclaration) {
        out += transpileEnumDeclaration((EnumDeclaration) ast) + "\n";
      }
    }

    for (Ast ast : declarations) {
      if (ast instanceof Declaration) {
        Declaration decl = (Declaration) ast;
        if (decl.getScoping() == VariableScoping.GLOBAL) {
          collectedGlobals.add(decl.getName());
        }
      }
    }
    for (Ast ast : declarations) {
      if (ast instanceof ClassDeclaration) {
        out += transpileClassDeclaration(program, (ClassDeclaration) ast) + "\n";
      } else if (ast instanceof Statement) {
        out += transpileStatement((Statement) ast) + "\n";
      } else if (ast instanceof Function) {
        throw new IllegalArgumentException("not expecting Function here, which is a statement");
      } else if (ast instanceof EnumDeclaration) {
        continue;
      }
    }
    return out;
  }

  private String transpileEnumDeclaration(EnumDeclaration ast) {
    String result = "class " + ast.enumType.name + "(Enum):\n";
    int iter = 0;
    for (String id : ast.enumIds) {
      result += INCREASE_INDENT + id + " = " + iter + "\n";
      iter += 1;
    }
    result += INCREASE_INDENT + "def copy(self):\n";
    result += INCREASE_INDENT + INCREASE_INDENT + "return self\n";
    return result + "\n";
  }

  private String transpileClassDeclaration(Program program, ClassDeclaration cdecl)
      throws TranspilerError, FatalMessageException {
    // fetch class header
    ClassHeader classHeader = (ClassHeader) project.lookupClassHeader(
        new ClassDeclType(cdecl.fullType.outerType, cdecl.fullType.name), Nullable.empty(), true,
        project.getAuditTrail());

    String result = "class " + cdecl.name;
    // ignore generics since python is dynamically typed...no matter what they
    // say...
    if (cdecl.parent.isNotNull()) {
      result += "(" + cdecl.parent.toString() + ")";
    }
    String saveIndent = indent;
    result += ":\n";
    indent += INCREASE_INDENT;

    result += indent + "def __repr__(self):\n" + indent + INCREASE_INDENT
        + "return \"<class instance>\"\n\n";

    // use this list to collect declarations
    List<DeclarationStatement> initFields = new LinkedList<>(); // just us
    List<DeclarationStatement> parentInitFields = new LinkedList<>(); // just our parents
    // collect functions as well, then replace all usages within the class with
    // self.<fn call>
    List<Function> fnFields = new LinkedList<>(); // just us
    List<Function> parentFnFields = new LinkedList<>(); // just our parents

    // collect parent declarations first
    /*
     * UserTypeEntry iterParent = null; if (cdecl.parent != null) {
     * Nullable<TypeBox> parentBox = null;
     * //globalTenv.lookupNestedClassname(cdecl.parent, -1, -1).get(); if
     * (parentBox.isNotNull()) { throw new
     * TranspilerError("internal: Unable to locate parent class declaration: " +
     * cdecl.parent); } if (!(parentBox.get().getValue().get() instanceof
     * UserTypeEntry)) { throw new TranspilerError("internal: parent '" + iterParent
     * + "' is not a class: " + parentBox.get().getValue()); } iterParent =
     * ((UserTypeEntry) parentBox.get().getValue().get()); }
     * 
     * 
     * while (iterParent != null) { ClassDeclaration parentDecl = iterParent.decl;
     * 
     * for (ClassLine cline : parentDecl.lines) { if (cline.ast instanceof
     * DeclarationStatement) { DeclarationStatement decl = (DeclarationStatement)
     * cline.ast; if (!decl.getIsStatic()) { parentInitFields.add(decl); } } else if
     * (cline.ast instanceof Function) { parentFnFields.add((Function) cline.ast); }
     * }
     * 
     * iterParent = iterParent.parent.get(); }
     */
    // then our declarations
    String hashFnName = "";
    String cmpFnName = "";
    for (ClassLine cline : cdecl.lines) {
      if (cline.ast instanceof DeclarationStatement) {
        DeclarationStatement decl = (DeclarationStatement) cline.ast;
        if (decl.getIsStatic()) {
          result += indent + transpileStatement(decl) + "\n";
        } else {
          initFields.add(decl);
        }
      } else if (cline.ast instanceof Function) {
        Function fn = (Function) cline.ast;
        if (fn.metaType == Function.MetaType.HASH) {
          hashFnName = fn.name;
        } else if (fn.metaType == Function.MetaType.CMP) {
          cmpFnName = fn.name;
        }
        fnFields.add(fn);
      }
    }

    // add an __init__ function
    result += indent + "def __init__(self):\n" + indent + INCREASE_INDENT + "super().__init__()\n";
    for (DeclarationStatement field : initFields) {
      result += indent + INCREASE_INDENT + "self." + field.name + " = None\n";
    }

    // add the vtable, if any (self.vtable = [self.fn])
    Nullable<ModuleEndpoint> modEp = project.lookupModule(program.moduleStatement.fullType,
        project.getAuditTrail());
    if (modEp.isNull()) {
      throw new TranspilerError(
          "internal error: failed to lookup module: " + program.moduleStatement.fullType);
    }
    try {
      modEp.get().load();
    } catch (FatalMessageException e) {
      throw new TranspilerError("internal error while loading module: " + e);
    }
    LoadedModuleHandle moduleHandle = (LoadedModuleHandle) modEp.get().getHandle();
    // find our vtable
    ClassDeclType absName = new ClassDeclType(cdecl.fullType.outerType, cdecl.fullType.name);
    ClassVtable vtable = moduleHandle.moduleBuildObj.vtables.get(absName);
    if (vtable == null) {
      throw new TranspilerError("internal error: Did not find vtable to copy for: " + absName);
    }

    if (!vtable.vtableEntries.isEmpty()) {
      result += indent + INCREASE_INDENT + "self.vtable = [";
      boolean first = true;
      for (VtableEntry entry : vtable.vtableEntries) {
        if (!first) {
          result += ", ";
        }
        first = false;
        result += "self." + entry.getName();
      }
      result += "]\n";
    }
    result += "\n";

    // add hashcode
    if (!hashFnName.isEmpty()) {
      result += indent + "def __hash__(self):                                            \n"
          + indent + INCREASE_INDENT + "return self." + hashFnName + "()                 \n"
          + indent + "\n";
    }
    // add equals function
    if (!cmpFnName.isEmpty()) {
      final String eqEnum = cmpResultType + "." + cmpEqId;
      result += indent + "def __eq__(self, obj):                                            \n"
          + indent + INCREASE_INDENT + "if not isinstance(obj, " + rootObjType + "):        \n"
          + indent + INCREASE_INDENT + INCREASE_INDENT + "return False                      \n"
          + indent + INCREASE_INDENT + "return self." + cmpFnName + "(obj) == " + eqEnum + "\n"
          + indent + "\n";
    }

    // add a copy function
    {
      // collect parents' declarations (all unique after renamings)
      result += indent + "def copy(self):\n" + indent + INCREASE_INDENT + "newInstance = "
          + cdecl.name + "()\n" + indent + INCREASE_INDENT + "self.copyInto(newInstance)\n" + indent
          + INCREASE_INDENT + "return newInstance\n\n";

      // add copyInto
      result += indent + "def copyInto(self, newInstance):\n";
      if (cdecl.parent.isNotNull()) {
        result += indent + INCREASE_INDENT + "super().copyInto(newInstance)\n";
      }
      result += indent + INCREASE_INDENT + "newInstance.typeStr = self.typeStr\n";
      for (DeclarationStatement field : initFields) {
        result += indent + INCREASE_INDENT + "newInstance." + field.name + " = self." + field.name;
        if (field.type instanceof UserInstanceType || field.type instanceof ArrayType
            || field.type instanceof MapType) {
          result += ".copy()";
        }
        result += "\n";
      }
      if (initFields.isEmpty()) {
        result += indent + INCREASE_INDENT + "pass\n";
      }
      result += "\n";
    }

    // find all init fields (this class and parents)
    List<ClassMember> allInitFields = new LinkedList<>();
    for (ClassMember member : classHeader.getMemberList()) {
      if (!member.isStatic && member.astType == AstType.DECLARATION_STATEMENT) {
        allInitFields.add(member);
      }
    }

    List<ClassMember> allFnFields = new LinkedList<>();
    for (ClassMember member : classHeader.getMemberList()) {
      if (!member.isStatic && member.astType == AstType.FUNCTION) {
        allFnFields.add(member);
      }
    }

    // transpile functions
    for (ClassLine cline : cdecl.lines) {
      if (cline.ast instanceof Function) {
        Function fn = (Function) cline.ast;
        if (fn.getIsStatic()) {
          result += indent + transpileFunction(fn) + "\n";
        } else {
          fn.params.add(0, new DeclarationStatement(0, 0, new LinkedList<>(), new LinkedList<>(),
              cdecl.fullType, "self", Nullable.empty(), false, false));
          // all non-params/decl names should be prepended with self.
          IdentifierExpression selfExpr = new IdentifierExpression(-1, -1, new LinkedList<>(),
              new LinkedList<>(), "self");

          // fields
          for (ClassMember decl : allInitFields) {
            IdentifierExpression targetExpr = new IdentifierExpression(-1, -1, new LinkedList<>(),
                new LinkedList<>(), decl.id);
            targetExpr.setDeterminedType(decl.getType());

            DotExpression withExpr = new DotExpression(-1, -1, new LinkedList<>(),
                new LinkedList<>(), selfExpr, targetExpr); // self.targetExpr
            withExpr.setDeterminedType(decl.getType());
            // fn.setBody((BlockStatement) fn.getBody().get().replace(targetExpr,
            // withExpr));

            LvalueId targetLv = new LvalueId(-1, -1, new LinkedList<>(), new LinkedList<>(),
                decl.id);
            LvalueDot withLv = new LvalueDot(-1, -1, new LinkedList<>(), new LinkedList<>(),
                selfExpr, decl.id);
            // fn.setBody((BlockStatement) fn.getBody().get().replace(targetLv, withLv));
          }

          // functions
          for (ClassMember decl : allFnFields) {
            IdentifierExpression targetExpr = new IdentifierExpression(-1, -1, new LinkedList<>(),
                new LinkedList<>(), decl.id);
            targetExpr.setDeterminedType(decl.getType());

            DotExpression withExpr = new DotExpression(-1, -1, new LinkedList<>(),
                new LinkedList<>(), selfExpr, targetExpr); // self.targetExpr
            withExpr.setDeterminedType(decl.getType());
            // fn.setBody((BlockStatement) fn.getBody().get().replace(targetExpr,
            // withExpr));

            LvalueId targetLv = new LvalueId(-1, -1, new LinkedList<>(), new LinkedList<>(),
                decl.id);
            LvalueDot withLv = new LvalueDot(-1, -1, new LinkedList<>(), new LinkedList<>(),
                selfExpr, decl.id);
            // fn.setBody((BlockStatement) fn.getBody().get().replace(targetLv, withLv));
          }

          if (fn.metaType == MetaType.CONSTRUCTOR) {
            // add parent constructor call
            if (fn.parentCall.isNotNull()) {
              // after the rename, the parentCall expression will be a regular id, not
              // "parent"

              if (fn.parentCall.get().prefix.isNotNull()) {
                throw new IllegalArgumentException();
              }
              FunctionCallExpression fnCallExpr = new FunctionCallExpression(Nullable.empty(),
                  fn.parentCall.get().expression, fn.parentCall.get().arguments,
                  fn.parentCall.get().originalName);
              fnCallExpr.setDeterminedType(VoidType.Create());
              FunctionCallStatement pFnCall = new FunctionCallStatement(fnCallExpr);
              pFnCall.setDeterminedType(VoidType.Create());

              fn.getBody().get().statements.add(0, pFnCall);
            }

            fn.getBody().get().statements.add(new ReturnStatement(-1, -1, new LinkedList<>(),
                new LinkedList<>(), Nullable.of(selfExpr)));
          }

          // then transpile fn
          result += indent + transpileFunction(fn);
        }
      } else if (cline.ast instanceof ClassDeclaration) {
        result += indent + transpileClassDeclaration(program, (ClassDeclaration) cline.ast);
      }
    }

    // add setRef function
    // we have to collect parent's members as well
    Map<String, Boolean> nameIsRef = new HashMap<String, Boolean>();
    result += indent + "def setRef(self, typeStr):\n";
    result += indent + INCREASE_INDENT + "self.nameIsRef = " + nameIsRef + "\n";
    result += indent + INCREASE_INDENT + "self.typeStr = typeStr\n";
    result += indent + INCREASE_INDENT + "return self\n";

    indent = saveIndent;
    return result;
  }

  private String transpileFunction(Function fn) throws TranspilerError {
    String result = "def ";
    result += fn.name + "(";

    boolean first = true;
    for (DeclarationStatement param : fn.params) {
      if (!first)
        result += ", ";
      first = false;
      result += param.name;
    }
    result += ")" + transpileBlockStatement(fn.getBody().get(), true);
    return result;
  }

  private String transpileMain(Program program) throws TranspilerError {
    String out = "def main():\n";

    Nullable<Function> main = program.getMainOrStart(Function.MetaType.START);
    if (main.isNull()) {
      return "";
    }

    out += indent + INCREASE_INDENT + main.get().name;
    if (main.get().fullType.argTypes.isEmpty()) {
      out += "()";
    } else {
      // TODO pass in command line
      String argv = "Custom_List(sys.argv).setRef(\"[string]\", False, False)";
      out += "(" + argv + ")";
    }
    out += "\n\n";
    return out;
  }

  private String transpileStatement(Statement ast) throws TranspilerError {
    if (!(ast instanceof AbstractStatement)) {
      throw new TranspilerError("transpileStatement ast is not an instance of AbstractStatement");
    }
    AbstractStatement node = (AbstractStatement) ast;
    switch (node.astType) {
      case FUNCTION:
        return transpileFunction((Function) ast) + "\n";
      case ASSIGNMENT_STATEMENT:
        return transpileAssignmentStatement((AssignmentStatement) node);
      case BLOCK_STATEMENT:
        return transpileBlockStatement((BlockStatement) node, false);
      case BREAK_STATEMENT:
        return "break";
      case CONDITIONAL_STATEMENT:
        return transpileConditionalStatement((ConditionalStatement) node, true);
      case CONTINUE_STATEMENT:
        return "continue";
      case DECLARATION_STATEMENT:
        return transpileDeclarationStatement((DeclarationStatement) node);
      case FOR_STATEMENT:
        return transpileForStatement((ForStatement) node);
      case FUNCTION_CALL_STATEMENT:
        return transpileFunctionCallExpression(
            (FunctionCallExpression) ((FunctionCallStatement) node).fnExpression);
      case RETURN_STATEMENT:
        return transpileReturnStatement((ReturnStatement) node);
      case WHILE_STATEMENT:
        return transpileWhileStatement((WhileStatement) node);
      case FFI_STATEMENT:
        return transpileFfiStatement((FfiStatement) node);
      case EXPRESSION_STATEMENT:
        return transpileExpression(((ExpressionStatement) node).expression);
      case COMMENT_STATEMENT:
        return transpileCommentStatement((CommentStatement) node);
      default:
        break;
    }
    return "# <Statement Error: " + node.astType + ">";
  }

  private String transpileCommentStatement(CommentStatement node) {
    if (!node.isBlockComment) {
      // trim off leading '//'
      return "#" + node.comment.substring(2);
    }
    return "\"\"\"" + node.comment + "\"\"\"";
  }

  private String transpileFfiStatement(FfiStatement node) {
    return node.literal.value;
  }

  private String transpileWhileStatement(WhileStatement node) throws TranspilerError {
    return "while (" + transpileExpression(node.guard) + ")" + transpileStatement(node.body);
  }

  private String transpileReturnStatement(ReturnStatement node) throws TranspilerError {
    String result = "return";
    if (node.returnValue.isNotNull()) {
      result += " " + transpileExpression(node.returnValue.get());
    }
    return result;
  }

  private String transpileForStatement(ForStatement node) throws TranspilerError {
    String result = "";

    if (node.init.isNotNull()) {
      result += transpileStatement(node.init.get()) + "\n"; // tack it on before the loop
    }

    List<Statement> guardStatements = new LinkedList<Statement>();
    guardStatements.add(new BreakStatement(-1, -1, new LinkedList<>(), new LinkedList<>()));
    BlockStatement guardBlock = new BlockStatement(-1, -1, new LinkedList<>(), new LinkedList<>(),
        guardStatements);

    node.body.statements.add(0, new ConditionalStatement(-1, -1, new LinkedList<>(),
        new LinkedList<>(), LeftUnaryOpExpression.createLeftUnaryOpExpression(-1, -1,
            new LinkedList<>(), new LinkedList<>(), "!", node.guard),
        guardBlock, Nullable.empty()));
    if (node.action.isNotNull()) {
      // then add it at the beginning of the loop body with a caveat
      // cannot add it at the end since 'continue' keyword

      // add this before the loop
      String tmpFirstId = "t_for_first_" + node.action.get().strLbl();
      result += indent + tmpFirstId + " = True\n";

      List<Statement> thenStatements = new LinkedList<Statement>();
      thenStatements.add(new AssignmentStatement(node.action.get().getLine(),
          node.action.get().getColumn(), new LinkedList<>(), new LinkedList<>(),
          new LvalueId(node.action.get().getLine(), node.action.get().getColumn(),
              new LinkedList<>(), new LinkedList<>(), tmpFirstId),
          new BoolExpression(node.action.get().getLine(), node.action.get().getColumn(),
              new LinkedList<>(), new LinkedList<>(), false)));
      BlockStatement thenBlock = new BlockStatement(node.action.get().getLine(),
          node.action.get().getColumn(), new LinkedList<>(), new LinkedList<>(), thenStatements);

      List<Statement> elsStatements = new LinkedList<Statement>();
      elsStatements.add(node.action.get());
      BlockStatement elsBlock = new BlockStatement(node.action.get().getLine(),
          node.action.get().getColumn(), new LinkedList<>(), new LinkedList<>(), elsStatements);

      ConditionalStatement tmpIf = new ConditionalStatement(node.action.get().getLine(),
          node.action.get().getColumn(), new LinkedList<>(), new LinkedList<>(),
          new IdentifierExpression(node.action.get().getLine(), node.action.get().getColumn(),
              new LinkedList<>(), new LinkedList<>(), tmpFirstId),
          thenBlock, Nullable.of(elsBlock));
      node.body.statements.add(0, tmpIf); // put at start of every loop run
    }

    result += indent + "while True" + transpileBlockStatement(node.body, false);
    return result;
  }

  private String transpileDeclarationStatement(DeclarationStatement node) throws TranspilerError {
    String result = node.name + " = ";

    if (node.getValue().isNotNull()) {
      result += attachCopyMethod(node.getValue().get().getDeterminedType(),
          transpileExpression(node.getValue().get()));
    } else {
      if (node.type instanceof ClassType) {
        result += node.type.toString() + "()";
      } else {
        result += "None";
      }
    }
    return result;
  }

  private String transpileConditionalStatement(ConditionalStatement node, boolean top)
      throws TranspilerError {
    String result = (top ? "" : "el") + "if (" + transpileExpression(node.guard) + ")";
    result += transpileStatement(node.thenBlock);
    if (node.elsBlock.isNotNull()) {
      if (node.elsBlock.instanceOf(ConditionalStatement.class)) {
        result += indent
            + transpileConditionalStatement((ConditionalStatement) node.elsBlock.get(), false);
      } else {
        result += indent + "else" + transpileStatement(node.elsBlock.get());
      }
    }
    return result;
  }

  private String transpileBlockStatement(BlockStatement node, boolean fnTop)
      throws TranspilerError {
    String result = ":\n";
    String save_indent = indent;
    indent += INCREASE_INDENT;

    if (fnTop) {
      result += printGlobals();
    }

    // new vars should be fine due to renaming which took care of shadowing

    for (Statement statement : node.statements) {
      result += indent + transpileStatement(statement) + "\n";
    }
    if (node.statements.isEmpty()) {
      result += indent + "pass";
    }
    result += "\n";
    indent = save_indent;
    return result;
  }

  private String transpileAssignmentStatement(AssignmentStatement node) throws TranspilerError {
    String targetStr = transpileLvalue(node.target);
    String sourceStr = attachCopyMethod(node.source.getDeterminedType(),
        transpileExpression(node.source));

    if (node.target instanceof LvalueDereference) {
      return "replace_with(" + targetStr + ", " + sourceStr + ")";
    }
    return targetStr + " = " + sourceStr;
  }

  private String transpileLvalue(Lvalue ast) throws TranspilerError {
    if (!(ast instanceof AbstractLvalue)) {
      throw new TranspilerError("transpileLvalue ast is not an instance of AbstractLvalue");
    }
    AbstractLvalue node = (AbstractLvalue) ast;
    switch (node.astType) {
      case ID_LVALUE:
        return transpileLvalueId((LvalueId) node);
      case BRACKET_LVALUE:
        return transpileLvalueBracket((LvalueBracket) node);
      case DOT_LVALUE:
        return transpileLvalueDot((LvalueDot) node);
      case DEREFERENCE_LVALUE:
        return transpileLvalueDereference((LvalueDereference) node);
      case FFI_LVALUE:
        return transpileLvalueFfi((LvalueFfi) node);
      case MULTI_LVALUE:
        return transpileMultiLvalue((MultiLvalue) node);
      default:
        break;
    }
    return "# <Expression Error: " + node.astType + ">";
  }

  private String transpileMultiLvalue(MultiLvalue node) throws TranspilerError {
    String res = "";
    boolean first = true;
    for (Lvalue lvalue : node.lvalues) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += transpileLvalue(lvalue);
    }
    return res;
  }

  private String transpileLvalueFfi(LvalueFfi node) {
    return node.literal.value;
  }

  private String transpileLvalueDot(LvalueDot node) throws TranspilerError {
    return transpileExpression(node.left) + "." + node.id;
  }

  private String transpileLvalueBracket(LvalueBracket node) throws TranspilerError {
    return transpileExpression(node.left) + "[" + transpileExpression(node.index) + "]";
  }

  private String transpileLvalueId(LvalueId node) throws TranspilerError {
    return node.id;
  }

  private String transpileLvalueDereference(LvalueDereference node) throws TranspilerError {
    // TODO add replaceWith somehow, probably in AssignmentStatement
    return transpileExpression(node.dereferenced);
  }

  private String transpileExpression(Ast ast) throws TranspilerError {
    if (!(ast instanceof AbstractExpression)) {
      throw new TranspilerError("transpileExpression ast is not an instance of AbstractExpression");
    }
    AbstractExpression node = (AbstractExpression) ast;
    switch (node.astType) {
      case ARRAY_INIT_EXPRESSION:
        return transpileArrayInitExpression((ArrayInitExpression) node);
      case BINARY_EXPRESSION:
        return transpileBinaryExpression((BinaryExpression) node);
      case BOOL_EXPRESSION:
        return transpileBoolExpression((BoolExpression) node);
      case BRACKET_EXPRESSION:
        return transpileBracketExpression((BracketExpression) node);
      case CHAR_EXPRESSION:
        return transpileCharExpression((CharExpression) node);
      case DOT_EXPRESSION:
        return transpileDotExpression((DotExpression) node);
      case DOUBLE_EXPRESSION:
        return transpileFloatExpression((FloatExpression) node);
      case FUNCTION_CALL_EXPRESSION:
        return transpileFunctionCallExpression((FunctionCallExpression) node);
      case IDENTIFIER_EXPRESSION:
        return transpileIdentifierExpression((IdentifierExpression) node);
      case INTEGER_EXPRESSION:
        return transpileIntegerExpression((IntegerExpression) node);
      case LAMBDA_EXPRESSION:
        return transpileLambdaExpression((LambdaExpression) node);
      case LEFT_UNARY_OP_EXPRESSION:
        return transpileLeftUnaryOpExpression((LeftUnaryOpExpression) node);
      case MAP_INIT_EXPRESSION:
        return transpileMapInitExpression((MapInitExpression) node);
      case NULL_EXPRESSION:
        return "None";
      case STRING_EXPRESSION:
        return ((StringExpression) node).unparsed;
      case REFERENCE_EXPRESSION:
        return transpileReferenceExpression(((ReferenceExpression) node));
      case DEREFERENCE_EXPRESSION:
        return transpileDereferenceExpression(((DereferenceExpression) node));
      case THIS_EXPRESSION:
        return "self";
      case VTABLE_ACCESS_EXPRESSION:
        return "vtable[" + ((VtableAccessExpression) node).index + "]";
      case NEW_CLASS_INSTANCE_EXPRESSION:
        return transpileNewClassInstanceExpression((NewClassInstanceExpression) node);
      case NEW_EXPRESSION:
        return transpileExpression(((NewExpression) node).value);
      case CAST_EXPRESSION:
        // TODO truncation?
        return transpileCastExpression((CastExpression) node);
      case FFI_EXPRESSION:
        return transpileFfiExpression((FfiExpression) node);
      case MULTI_EXPRESSION:
        return transpileMultiExpression((MultiExpression) node);
      case STATIC_CLASS_IDENTIFIER_EXPRESSION:
        return ((StaticClassIdentifierExpression) node).id.toString();
      case SIZEOF_EXPRESSION:
        return ((SizeofExpression) node).getCalculatedSize() + "";
      case INSTANCEOF_EXPRESSION:
        return transpileInstanceofExpression((InstanceofExpression) node);
      default:
        break;
    }
    return "# <Expression Error: " + node.astType + ">";
  }

  private String transpileInstanceofExpression(InstanceofExpression node) throws TranspilerError {
    String result = "";
    switch (node.operator) {
      case INSTANCEOF:
        break;
      case NOT_INSTANCEOF:
        result += "!";
        break;
      default:
        throw new TranspilerError("unknown operator: " + node.operator);
    }
    result += "(";
    result += "isinstance(" + transpileExpression(node.left) + ", " + transpileType(node.right)
        + ")";
    result += ")";
    return "(" + result + ")";
  }

  private String transpileCastExpression(CastExpression node) throws TranspilerError {
    if (node.target.equals(CharType.Create())) {
      return "custom_chr(" + transpileExpression(node.value) + ")";
    }
    return transpileExpression(node.value);
  }

  private String transpileMultiExpression(MultiExpression node) throws TranspilerError {
    String res = "";
    boolean first = true;
    for (Expression expr : node.expressions) {
      if (!first) {
        res += ", ";
      }
      first = false;
      res += transpileExpression(expr);
    }
    return res;
  }

  private String transpileFfiExpression(FfiExpression node) {
    return node.literal.value;
  }

  private String transpileReferenceExpression(ReferenceExpression node) throws TranspilerError {
    return transpileExpression(node.referenced);
  }

  private String transpileDereferenceExpression(DereferenceExpression node) throws TranspilerError {
    return transpileExpression(node.dereferenced);
  }

  private String transpileNewClassInstanceExpression(NewClassInstanceExpression node)
      throws TranspilerError {
    String nameStr = "";
    UserInstanceType tmp = node.getName();
    List<String> nameList = tmp.toList();

    nameStr = String.join(".", nameList);
    return nameStr + "()." + transpileExpression(node.constructorCall) + ".setRef(\""
        + node.originalName + "\")";
  }

  private String transpileDotExpression(DotExpression node) throws TranspilerError {
    return transpileExpression(node.left) + "." + transpileExpression(node.right);
  }

  private String transpileMapInitExpression(MapInitExpression node) throws TranspilerError {
    String result = "{";
    boolean first = true;
    for (Entry<Expression, Expression> pair : node.pairs.entrySet()) {
      if (!first)
        result += ",\n";
      first = false;

      result += attachCopyMethod(pair.getKey().getDeterminedType(),
          transpileExpression(pair.getKey()));
      result += ": ";
      result += attachCopyMethod(pair.getValue().getDeterminedType(),
          transpileExpression(pair.getValue()));
    }
    result = result + "}";
    return "Custom_Dict(" + result + ").setRef(\"" + node.getDeterminedType() + "\", "
        + (node.keyType instanceof ReferenceType ? "True" : "False") + ", "
        + (node.valueType instanceof ReferenceType ? "True" : "False") + ", "
        + (isFullCopy(node.keyType) ? "True" : "False") + ", "
        + (isFullCopy(node.valueType) ? "True" : "False") + ")";
  }

  private String transpileLeftUnaryOpExpression(LeftUnaryOpExpression node) throws TranspilerError {
    String op = node.operator.leftUnaryOpName;
    if (op.equals("!")) {
      op = "not";
    }

    return "(" + op + "(" + transpileExpression(node.right) + "))";
  }

  private String transpileLambdaExpression(LambdaExpression node) throws TranspilerError {
    String result = "";
    /* capture by copy */;

    // get free variables and add them as parameters, unless they are globals
    Map<String, Type> freeVars = node.findFreeVariables();
    for (String global : collectedGlobals) {
      freeVars.remove(global);
    }

    // need to do ast replace on free variables in lambda body (e.g. freeVar ->
    // clo.freeVar)
    BlockStatement newBody = node.body; // replace free vars with clo.fv
    IdentifierExpression cloExpr = new IdentifierExpression(-1, -1, new LinkedList<>(),
        new LinkedList<>(), "clo");
    for (Entry<String, Type> fv : freeVars.entrySet()) {
      // replace expressions
      IdentifierExpression targetExpr = new IdentifierExpression(-1, -1, new LinkedList<>(),
          new LinkedList<>(), fv.getKey());
      targetExpr.setDeterminedType(fv.getValue());
      DotExpression withExpr = new DotExpression(-1, -1, new LinkedList<>(), new LinkedList<>(),
          cloExpr, targetExpr);
      withExpr.setDeterminedType(fv.getValue());
      newBody = (BlockStatement) newBody.replace(targetExpr, withExpr);

      // and value updates
      LvalueId targetLvalue = new LvalueId(-1, -1, new LinkedList<>(), new LinkedList<>(),
          fv.getKey());
      targetLvalue.setDeterminedType(fv.getValue());
      LvalueDot withLvalue = new LvalueDot(-1, -1, new LinkedList<>(), new LinkedList<>(), cloExpr,
          fv.getKey());
      withLvalue.setDeterminedType(fv.getValue());
      newBody = (BlockStatement) newBody.replace(targetLvalue, withLvalue);
    }

    List<DeclarationStatement> fakeParams = new LinkedList<DeclarationStatement>(node.params);
    fakeParams.add(new DeclarationStatement(node.getLine(), node.getColumn(), new LinkedList<>(),
        new LinkedList<>(), VoidType.Create(), "clo", Nullable.empty(), false, false));

    // create tmp top-level python fn
    String tmpFnName = "py_lam_" + node.strLbl();

    String saveIndent = indent;
    indent = "";

    String topResult = "def " + tmpFnName + "(";
    String fakeParamsArgs = "";

    boolean first = true;
    for (DeclarationStatement param : fakeParams) {
      if (!first) {
        topResult += ", ";
        fakeParamsArgs += ", ";
      }
      first = false;

      String tmp = param.name;
      topResult += tmp;

      if (param.type != null) {
        fakeParamsArgs += attachCopyMethod(param.type, param.name);
      } else {
        // only the clo arg
        fakeParamsArgs += param.name;
      }
    }

    topResult += ")" + transpileBlockStatement(newBody, true);
    topLevelFuncs += "\n" + topResult;

    indent = saveIndent;
    // wrap fn call in capturing python lambda
    // (lambda fv1, fv2: (lambda p1, p2: tmpFn(p1, p2, fv1, fv2)))(fv1, fv2)
    // (lambda clo: (lambda p1, p2: tmpFn(p1, p2, clo)))(Namespace(fv1=fv1,
    // fv2=fv2))

    String freeVarsArgs = "Namespace(";
    result += "(lambda clo"; // outer lambda with free variables

    first = true;
    for (Entry<String, Type> fv : freeVars.entrySet()) {
      if (!first) {
        freeVarsArgs += ", ";
      }
      first = false;
      String fvName = fv.getKey();
      if (fvName.equals("print")) {
        fvName = "custom_print";
      }
      freeVarsArgs += fvName + '=' + attachCopyMethod(fv.getValue(), fvName);
    }
    freeVarsArgs += ')';

    result += ": (lambda "; // inner lambda with params
    first = true;
    for (DeclarationStatement param : node.params) {
      if (!first)
        result += ", ";
      first = false;

      result += param.name;
    }

    // actual fn call
    result += ": " + tmpFnName + "(" + fakeParamsArgs + ")))(" + freeVarsArgs + ")";

    return result;
  }

  private String transpileIntegerExpression(IntegerExpression node) {
    return node.value;
  }

  private String transpileIdentifierExpression(IdentifierExpression node) throws TranspilerError {
    if (node.id.equals("print")) {
      return "custom_print";
    }
    return node.id;
  }

  private String transpileFunctionCallExpression(FunctionCallExpression node)
      throws TranspilerError {
    String result = "";

    if (node.prefix.isNotNull()) {
      Expression prefixExpr = node.prefix.get();
      String dot = ".";

      Type prefixType = prefixExpr.getDeterminedType();
      if (prefixType instanceof ReferenceType) {
        prefixType = ((ReferenceType) prefixType).innerType;
      }

      if ((prefixType instanceof ArrayType || prefixType instanceof MapType)
          && node.expression instanceof IdentifierExpression) {

        String name = ((IdentifierExpression) node.expression).id;
        if (name.equals("remove")) {
          name = "pop";
        }
        if (name.equals("size")) {
          return "len(" + transpileExpression(prefixExpr) + ")";
        }
        if (name.equals("add")) {
          if (node.arguments.size() == 1) {
            name = "append";
          } else {
            name = "insert";
          }
        }
        if (name.equals("put")) {
          return transpileExpression(prefixExpr) + "[" + transpileExpression(node.arguments.get(0))
              + "]" + " = " + transpileExpression(node.arguments.get(1));
        }

        String argStr = "(";
        boolean first = true;
        for (Expression arg : node.arguments) {
          if (!first) {
            argStr += ", ";
          }
          first = false;
          argStr += transpileExpression(arg);
        }

        return transpileExpression(prefixExpr) + "." + name + argStr + ")";
      }
      result += transpileExpression(prefixExpr) + dot;
    }

    result += transpileExpression(node.expression) + "(";

    boolean first = true;
    for (Expression arg : node.arguments) {
      if (!first)
        result += ", ";
      first = false;

      result += attachCopyMethod(arg.getDeterminedType(), transpileExpression(arg));
    }
    return result + ")";
  }

  private String transpileFloatExpression(FloatExpression node) {
    return node.value;
  }

  private String transpileCharExpression(CharExpression node) {
    return node.unparsed;
  }

  private String transpileBracketExpression(BracketExpression node) throws TranspilerError {
    return transpileExpression(node.left) + "[" + transpileExpression(node.index) + "]";
  }

  private String transpileBoolExpression(BoolExpression node) {
    return node.value ? "True" : "False";
  }

  private String transpileBinaryExpression(BinaryExpression node) throws TranspilerError {
    String op = node.operator.binOpName;
    boolean ffiConcat = false;
    boolean castInt = false;
    if (op.equals("&&")) {
      op = "and";
    } else if (op.equals("||")) {
      op = "or";
    } else if (op.equals("#")) {
      ffiConcat = true;
    }

    String result = "";
    String suffix = "";
    if (node.left.getDeterminedType() instanceof ReferenceType
        && node.right.getDeterminedType() instanceof ReferenceType) {
      if (op.equals("==")) {
        op = "is";
      } else if (op.equals("!=")) {
        result += "(not";
        suffix = ")";
        op = "is";
      }
    }

    result += "(";
    String tmp = transpileExpression(node.left);
    if (!ffiConcat) {
      if (node.right.getDeterminedType().equals(StringType.Create())
          && !node.left.getDeterminedType().equals(StringType.Create())) {
        result += wrapInToString(tmp, node.left);
      } else if (op.equals("+") && !node.right.getDeterminedType().equals(CharType.Create())
          && !node.right.getDeterminedType().equals(StringType.Create())
          && node.left.getDeterminedType().equals(CharType.Create())) {
        result += "custom_ord(" + tmp + ")";
      } else if ("-%".contains(op) && node.left.getDeterminedType().equals(CharType.Create())) {
        result += "custom_ord(" + tmp + ")";
      } else {
        result += tmp;
      }
    } else {
      result += tmp;
    }

    if (!ffiConcat) {
      result += " " + op + " ";
    }

    tmp = transpileExpression(node.right);
    if (!ffiConcat && !castInt) {
      if (node.left.getDeterminedType().equals(StringType.Create())
          && !node.right.getDeterminedType().equals(StringType.Create())) {
        result += wrapInToString(tmp, node.right);
      } else if (op.equals("+") && !node.left.getDeterminedType().equals(CharType.Create())
          && !node.left.getDeterminedType().equals(StringType.Create())
          && node.right.getDeterminedType().equals(CharType.Create())) {
        result += "custom_ord(" + tmp + ")";
      } else if ("-%".contains(op) && node.right.getDeterminedType().equals(CharType.Create())) {
        result += "custom_ord(" + tmp + ")";
      } else {
        result += tmp;
      }
    } else {
      result += tmp;
    }

    return result + ")" + suffix;
  }

  private String wrapInToString(String value, Ast node) {
    return "to_string(" + value + ", "
        + (node.getDeterminedType() instanceof ReferenceType ? "True" : "False") + ")";
  }

  private String transpileArrayInitExpression(ArrayInitExpression node) throws TranspilerError {
    String result = "[";
    boolean first = true;
    for (Expression val : node.values) {
      if (!first)
        result += ", ";
      first = false;

      result += attachCopyMethod(val.getDeterminedType(), transpileExpression(val));
    }
    result = result + "]";
    return "Custom_List(" + result + ").setRef(\"" + node.getDeterminedType() + "\", "
        + (node.elementType instanceof ReferenceType ? "True" : "False") + ", "
        + (isFullCopy(node.elementType) ? "True" : "False") + ")";
  }

  private boolean isFullCopy(Type type) {
    return (type instanceof UserInstanceType || type instanceof MapType
        || type instanceof ArrayType);
  }

  private String attachCopyMethod(Type type, String expr) {
    if (isFullCopy(type)) {
      return "((" + expr + ").copy())";
    }
    return expr;
  }

  ////////////////// transpile type

  public static String transpileType(Type type) {
    if (!(type instanceof AbstractType)) {
      throw new IllegalArgumentException("transpileType type is not an instance of AbstractType");
    }
    String result = "";
    FunctionType fTy = null;
    ClassType uTy = null;
    ClassDeclType declTy = null;
    EnumType enumTy = null;
    EnumDeclType enumDeclTy = null;
    boolean first = true;

    AbstractType aType = (AbstractType) type;
    switch (aType.typeType) {
      case VOID_TYPE:
      case BOOL_TYPE:
        return aType.toString().replaceAll("const ", "");
      case CHAR_TYPE:
        return "bytes";
      case INT_TYPE:
        return "int";
      case FLOAT32_TYPE:
        return "float";
      case FLOAT64_TYPE:
        return "double";
      case STRING_TYPE:
        return "str";
      case ARRAY_TYPE:
        return "list";
      case MAP_TYPE:
        return "dict";
      case FUNCTION_TYPE:
        throw new UnsupportedOperationException();
      case REFERENCE_TYPE:
        result = transpileType(((ReferenceType) aType).innerType);
        return result;
      case CLASS_DECL_TYPE:
        declTy = (ClassDeclType) aType;
        result = "";

        if (declTy.outerType.isNotNull()) {
          result += transpileType(declTy.outerType.get()) + ".";
        }

        result += declTy.name;
        return result;
      case CLASS_TYPE:
        uTy = (ClassType) aType;
        result = "";

        if (uTy.outerType.isNotNull()) {
          result += transpileType(uTy.outerType.get()) + ".";
        }

        result += uTy.name;
        return result; // this replace is for any nested types
      case ENUM_DECL_TYPE:
        enumDeclTy = (EnumDeclType) aType;
        result = "";

        if (enumDeclTy.outerType.isNotNull()) {
          result += transpileType(enumDeclTy.outerType.get()) + ".";
        }

        result += enumDeclTy.name;
        return result;
      case ENUM_TYPE:
        enumTy = (EnumType) aType;
        result = "";

        if (enumTy.outerType.isNotNull()) {
          result += transpileType(enumTy.outerType.get()) + ".";
        }

        result += enumTy.name;
        return result;
      case MULTI_TYPE:
        return transpileMultiType((MultiType) type);
      case MODULE_TYPE:
        return transpileModuleType((ModuleType) type);
      default:
        break;
    }
    return "/*<Error: " + aType.typeType + ">*/";
  }

  public static String transpileModuleType(ModuleType type) {
    List<String> names = type.toList();
    return String.join(".", names);
  }

  public static String transpileMultiType(MultiType type) {
    String types = "(";
    boolean first = true;
    for (Type ty : type.types) {
      if (!first) {
        types += ", ";
      }
      first = false;
      types += transpileType(ty);
    }
    return types + ")";
  }
}
