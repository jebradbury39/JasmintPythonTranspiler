package transpiler;

import ast.Ast;
import ast.SerializationError;
import errors.FatalMessageException;
import interp.InterpError;
import java.io.IOException;
import java.util.Optional;
import multifile.LoadedModuleHandle;
import multifile.ModuleEndpoint;
import multifile.Project;
import threading.ThreadManager;

public class TranspileProject {

  private final String builtinFname;
  private final String astFname;

  public TranspileProject(String builtinFname, String astFname) {
    this.builtinFname = builtinFname;
    this.astFname = astFname;
  }

  @SuppressWarnings("resource")
  public void run()
      throws InterpError, FatalMessageException, InterruptedException, TranspilerError {
    System.out.println("Running python_transpiler: " + astFname);

    Project project = null;
    Optional<Project> optProj = Optional.empty();
    boolean hitErr = false;
    try {
      project = Project.deserializeFromFile(astFname, "build-jsmnt-py");
    } catch (IOException | SerializationError e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }

    ThreadManager threadManager = new ThreadManager(1);

    project.linkModules(threadManager);

    threadManager.stop();

    optProj = project.linkRequiredProvidedFunctions("Python");
    if (optProj.isPresent()) {
      project = optProj.get();
    } else {
      hitErr = true;
    }

    if (!hitErr) {
      optProj = project.resolveUserTypes();
      if (optProj.isPresent()) {
        project = optProj.get();
      } else {
        hitErr = true;
      }
    }

    if (!hitErr) {
      project = project.normalizeTypes(false); // make all types abs at first

      project = project.removeNonModuleImports();
    }

    if (!hitErr) {
      project = project.insertStartFunction();
    }

    if (!hitErr) {
      project = project.liftClassDeclarations();
    }

    // project = project.shuffleClassDeclarationsToTop();
    if (!hitErr) {
      project.typecheck();
    }

    // run renaming to remove shadowing
    // project = project.renameIds().get();

    // make sure we have no parameterized types (so no templates)
    if (!hitErr) {
      optProj = project.resolveGenerics();
      if (optProj.isPresent()) {
        project = optProj.get();
      } else {
        hitErr = true;
      }
    }

    if (!hitErr) {
      project.typecheck();
    }

    // move inits to constructors for self
    if (!hitErr) {
      project = project.moveInitsToConstructors();
    }

    if (!hitErr) {
      optProj = project.renameIds();
      if (optProj.isPresent()) {
        project = optProj.get();
      } else {
        hitErr = true;
      }
    }

    project = project.insertDestructorCalls();

    if (!hitErr) {
      optProj = project.toAnf(Ast.AnfTune.T0);
      if (optProj.isPresent()) {
        project = optProj.get();
      } else {
        hitErr = true;
      }
    }

    if (!hitErr) {
      optProj = project.resolveExceptions();
      if (optProj.isPresent()) {
        project = optProj.get();
      } else {
        hitErr = true;
      }
    }

    if (!hitErr) {
      optProj = project.renameIds();
      if (optProj.isPresent()) {
        project = optProj.get();
      } else {
        hitErr = true;
      }
    }

    if (!hitErr) {
      project = project.renameTypes();

      project = project.addThisExpression();

      project = project.normalizeTypes(true); // now make types relative
    }

    if (!hitErr) {
      optProj = project.resolveVtables();
      if (optProj.isPresent()) {
        project = optProj.get();
      } else {
        hitErr = true;
      }
    }

    // this also restores determinedTypes
    if (!hitErr) {
      project.typecheck();
    }

    if (project.hitError() || hitErr) {
      System.out.println(project.errorReport());
      return;
    }

    try {
      transpileProject(project);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /*
   * Split up each module into a header and source file. Then 'import std' becomes
   * '#include "std.hpp";' This requires no real knowledge of the other modules,
   * since every module path will be unique and each program only needs to handle
   * the import and submod stuff.
   */
  private void transpileProject(Project project)
      throws IOException, FatalMessageException, TranspilerError {
    for (Project lib : project.getBuildLibs()) {
      transpileProject(lib);
    }

    for (ModuleEndpoint mod : project.getSources()) {
      mod.load();
      LoadedModuleHandle moduleHandle = (LoadedModuleHandle) mod.getHandle();
      TranspileProgram tp = new TranspileProgram(builtinFname, project,
          moduleHandle.moduleBuildObj.program, moduleHandle.moduleBuildObj.effectiveImports
              .applyTransforms(project.getAuditTrail(), mod.msgState));
      tp.transpileProgram();
    }
  }
}
