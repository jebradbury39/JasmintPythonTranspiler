package transpiler;

public class TranspilerError extends Exception {

  public TranspilerError(String msg) {
    super("PythonTranspiler Error: " + msg);
  }
}
